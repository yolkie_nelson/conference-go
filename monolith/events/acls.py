import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}

    base_url = "https://api.pexels.com/v1/search"
    params = {
        "query": f"{city} {state}",
    }

    response = requests.get(base_url, headers=headers, params=params)

    content = response.content
    photo = json.loads(content)
    picture = photo["photos"][3]["src"]["original"]
    try:
        return {"picture_url": picture}
    except (KeyError, IndexError):
        return {"picture_url": None}


    # if response.status_code == 200:
    #     data = response.json()
    #     if "photos" in data and data["photos"]:
    #         return {"picture_url": data["photos"][0]["src"]["original"]}

    # return {"picture_url": None}



def get_lat_lon(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},USA",
        "appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url, params=params)
    parsed_json = json.loads(response.content)
    try:
        return {
            "latitude": parsed_json[0]["lat"],
            "longitude": parsed_json[0]["lon"],
        }
    except (KeyError, IndexError):
        return {"url": None}


def get_weather_data(city, state):
    lat_lon = get_lat_lon(city, state)
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat_lon["latitude"],
        "lon": lat_lon["longitude"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, params=params)
    parsed_json = json.loads(response.content)
    weather_data = {
        "temp": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"],
    }
    try:
        return weather_data
    except (KeyError, IndexError):
        return {"url": None}

    # weather_url = "https://api.openweathermap.org/data/2.5/weather"
    # weather_params = {"q": f"{city},{state}", "appid": OPEN_WEATHER_API_KEY}
    # weather_data = requests.get(weather_url, params=weather_params).json()

    # if weather_data.get("cod") == 200:
    #     main_data = weather_data.get("main", {})
    #     weather_description = weather_data.get("weather", [{}])[0].get("description", "")

    #     return {"temperature": main_data.get("temp", ""), "description": weather_description}

    # return None
